import { panelTitle } from "./ObjectUtils";
import { StageObject } from "./StageObject";
import monoColor from '@/assets/icons/black-color.svg'
import linearColor from '@/assets/icons/linear-gradient.svg'
import radialColor from '@/assets/icons/radial-gradient.svg'

export const ColorList = [
    {
        title: '单色',
        key: 'color',
        ico: monoColor
    }, {
        title: '线性',
        key: 'linear',
        ico: linearColor
    }, {
        title: '径向',
        key: 'radial',
        ico: radialColor
    }
]
/**
 * 颜色对象
 */
export class ColorObject extends StageObject {
    public colorType = 'color';
    /**单色 */
    public mono = false;
}
export class SvgColor extends ColorObject {
    private _color = '#000';
    public colorType = 'color'
    constructor(color?: string) {
        super()
        if (color) {
            this._color = color;
        }
    }
    get value() {
        return this._color;
    }
    set value(val: string) {
        this._color = val;
    }
    toString() {
        return this._color;
    }
    get cssColor(){
        return this._color;
    }
}
/**
 * 填充图案
 */
export class PatternObject extends ColorObject {
    @panelTitle('视图大小')
    public viewBox: string = '0 0 500 500';
    @panelTitle('宽度')
    public width: number = 50;
    @panelTitle('高度')
    public height: number = 50;
    public name = '填充图案';
    get value() {
        return `url('#${this.id}')`;
    }
}
/**
 * 渐变颜色
 */
export class StopObject extends ColorObject {
    @panelTitle('偏移%')
    public offset: number = 0;
    @panelTitle('颜色')
    public stopColor: SvgColor = new SvgColor('');
    @panelTitle('透明度')
    public stopOpacity: number = 1;

    static fromNode(node: SVGElement) {
        const stop = new StopObject();
        const id = node.getAttribute('id');
        if(id){
            stop.id = id;
        }
        const offset = node.getAttribute('offset') || '0';
        if(offset.indexOf('%') > 0){
            stop.offset = parseFloat(offset.replace('%',''));
        }else{
            stop.offset = parseFloat(offset) * 100;
        }
        
        stop.stopColor = new SvgColor(node.getAttribute('stop-color') || 'black');
        return stop;
    }
}
/**
 * 径向渐变
 */
export class RadialGradient extends ColorObject {
    @panelTitle('终点X%')
    public cx: number = 50;
    @panelTitle('终点Y%')
    public cy: number = 50;
    @panelTitle('起点X%')
    public fx: number = 50;
    @panelTitle('起点Y%')
    public fy: number = 50;
    @panelTitle('半径')
    public fr: number = 50;
    public name = '径向渐变';

    public children: StopObject[] = [];
    public colorType = 'radial'

    get value() {
        return `url('#${this.id}')`;
    }
    get cssColor() {
        let s = ''
        for (const c of this.children) {
            const color = c as StopObject;
            if (s == '') {
                s += color.stopColor.value + ' ' + color.offset + '%';
            } else {
                s += ',' + color.stopColor.value + ' ' + color.offset + '%';
            }

        }
        return 'linear-gradient(to right,' + s + ')'
    }

    static fromNode(node: SVGElement) {
        const linearGradient = new RadialGradient();
        const id = node.getAttribute('id');
        if(id){
            linearGradient.id = id;
        }
        
        return linearGradient;
    }
}

export class LinearGradient extends ColorObject {
    public colorType = 'linear'
    public name = '线性渐变';
    public rotate = 0;
    public x1 = '';
    public y1 = '';
    public x2 = '';
    public y2 = '';
    public gradientUnits = 'objectBoundingBox';//objectBoundingBox userSpaceOnUse
    public get gradientTransform() {
        return 'rotate(' + this.rotate + ')'
    }
    get value() {
        return `url('#${this.id}')`;
    }
    get cssColor() {
        let s = ''
        for (const c of this.children) {
            const color = c as StopObject;
            if (s == '') {
                s += color.stopColor.value + ' ' + color.offset + '%';
            } else {
                s += ',' + color.stopColor.value + ' ' + color.offset + '%';
            }

        }
        return 'linear-gradient(to right,' + s + ')'
    }
    static fromNode(node: SVGElement) {
        const linearGradient = new LinearGradient();
        const trans =  node.getAttribute('gradientTransform')
        if(trans){
            const r = trans.match(/rotate\((\d+)\)/)
            if(r){
                linearGradient.rotate = Number(r[1])
            }
        }
        const gradientUnits = node.getAttribute('gradientUnits');
        if(gradientUnits){
            linearGradient.gradientUnits = gradientUnits;
        }
        const x1 = node.getAttribute('x1');
        if(x1){
            linearGradient.x1 = x1;
        }
        const y1 = node.getAttribute('y1');
        if(y1){
            linearGradient.y1 = y1;
        }
        const x2 = node.getAttribute('x2');
        if(x2){
            linearGradient.x2 = x2;
        }
        const y2 = node.getAttribute('y2');
        if(y2){
            linearGradient.y2 = y2;
        }
        const id = node.getAttribute('id');
        if(id){
            linearGradient.id = id;
        }
        return linearGradient;
    }
}
